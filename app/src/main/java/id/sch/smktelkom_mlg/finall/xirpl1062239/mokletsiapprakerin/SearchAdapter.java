package id.sch.smktelkom_mlg.finall.xirpl1062239.mokletsiapprakerin;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SearchViewHolder> {
    Context context;
    ArrayList<String> namaList;
    ArrayList<String> alamatList;

    public SearchAdapter(Context context, ArrayList<String> namaList, ArrayList<String> alamatList) {
        this.context = context;
        this.namaList = namaList;
        this.alamatList = alamatList;
    }

    @NonNull
    @Override
    public SearchAdapter.SearchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.perusahaan_list, parent, false);
        return new SearchAdapter.SearchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchViewHolder holder, int position) {
        holder.tvNama.setText(namaList.get(position));
        holder.tvAlamat.setText(alamatList.get(position));
    }

    @Override
    public int getItemCount() {
        return namaList.size();
    }

    class SearchViewHolder extends RecyclerView.ViewHolder {
        TextView tvNama, tvAlamat;

        public SearchViewHolder(View itemView) {
            super(itemView);
            tvNama = itemView.findViewById(R.id.tvNama);
            tvAlamat = itemView.findViewById(R.id.tvAlamat);
        }
    }
}
