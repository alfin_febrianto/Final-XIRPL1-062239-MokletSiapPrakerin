package id.sch.smktelkom_mlg.finall.xirpl1062239.mokletsiapprakerin;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import id.sch.smktelkom_mlg.finall.xirpl1062239.mokletsiapprakerin.Model.Perusahaan;

public class MainActivity extends AppCompatActivity {

    FirebaseUser firebaseUser;
    EditText search_edit_text;
    ArrayList<String> namaList;
    ArrayList<String> alamatList;
    SearchAdapter searchAdapter;
    private RecyclerView recyclerView;
    private DatabaseReference myref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        search_edit_text = findViewById(R.id.search_edit_text);
        recyclerView = findViewById(R.id.recyclerview);

        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        myref = FirebaseDatabase.getInstance().getReference().child("/Perusahaan");
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        namaList = new ArrayList<>();
        alamatList = new ArrayList<>();

        search_edit_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().isEmpty()) {
                    setAdapter(s.toString());
                }
            }
        });

        FirebaseRecyclerAdapter<Perusahaan, PerusahaanViewHolder>
                recyclerAdapter = new FirebaseRecyclerAdapter<Perusahaan, PerusahaanViewHolder>(
                Perusahaan.class,
                R.layout.perusahaan_list,
                PerusahaanViewHolder.class,
                myref
        ) {
            @Override
            protected void populateViewHolder(PerusahaanViewHolder viewHolder, Perusahaan model, int position) {
                viewHolder.setNama(model.getNama());
                viewHolder.setAlamat(model.getAlamat());
            }
        };

        recyclerView.setAdapter(recyclerAdapter);

    }

    private void setAdapter(final String searchedstring) {

        myref.child("Perusahaan").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int counter = 0;
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    String uid = snapshot.getKey();
                    String nama = snapshot.child("nama").getValue(String.class);
                    String alamat = snapshot.child("alamat").getValue(String.class);

                    if (nama.contains(searchedstring)) {
                        namaList.add(nama);
                        alamatList.add(alamat);
                        counter++;
                    } else if (alamat.contains(searchedstring)) {
                        namaList.add(nama);
                        alamatList.add(alamat);
                        counter++;
                    }
                    if (counter == 15)
                        break;
                }
                searchAdapter = new SearchAdapter(MainActivity.this, namaList, alamatList);
                recyclerView.setAdapter(searchAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static class PerusahaanViewHolder extends RecyclerView.ViewHolder {
        View mView;
        TextView tvnama;
        TextView tvalamat;

        public PerusahaanViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            tvnama = itemView.findViewById(R.id.tvNama);
            tvalamat = itemView.findViewById(R.id.tvAlamat);
        }

        public void setNama(String nama) {
            tvnama.setText(nama);
        }

        public void setAlamat(String alamat) {
            tvalamat.setText(alamat);
        }
    }

}
