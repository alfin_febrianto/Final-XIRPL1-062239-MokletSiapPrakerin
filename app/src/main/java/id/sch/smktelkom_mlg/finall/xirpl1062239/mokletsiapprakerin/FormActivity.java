package id.sch.smktelkom_mlg.finall.xirpl1062239.mokletsiapprakerin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import id.sch.smktelkom_mlg.finall.xirpl1062239.mokletsiapprakerin.Model.Form;

/**
 * Created by Alfin Febrianto on 08/04/2018.
 */

public class FormActivity extends AppCompatActivity {

    EditText etnama, etnis, etalamat, ettelp, etemail;
    Spinner spkelas, sptipe;
    Button buttonAdd;


    DatabaseReference databasePerusahaan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_page);

        etnama = findViewById(R.id.et_nama);
        etnis = findViewById(R.id.et_nis);
        etalamat = findViewById(R.id.et_alamat);
        ettelp = findViewById(R.id.et_telp);
        etemail = findViewById(R.id.et_email);
        spkelas = findViewById(R.id.sp_kelas);
        sptipe = findViewById(R.id.sp_tipe);
        buttonAdd = findViewById(R.id.buttonAdd);

        databasePerusahaan = FirebaseDatabase.getInstance().getReference("Form");

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addData();
            }
        });

    }


    private void addData() {
        String nama = etnama.getText().toString().trim();
        String nis = etnis.getText().toString().trim();
        String alamat = etalamat.getText().toString().trim();
        String telp = ettelp.getText().toString().trim();
        String email = etemail.getText().toString().trim();
        String kelas = spkelas.getSelectedItem().toString().trim();
        String tipe = sptipe.getSelectedItem().toString().trim();

        if (TextUtils.isEmpty(nama)) {
            Toast.makeText(this, "Isi Data Nama", Toast.LENGTH_LONG).show();
        } else if (TextUtils.isEmpty(nis)) {
            Toast.makeText(this, "Isi Data NIS", Toast.LENGTH_LONG).show();
        }else if (TextUtils.isEmpty(alamat)) {
            Toast.makeText(this, "Isi Data Alamat", Toast.LENGTH_LONG).show();
        } else if (TextUtils.isEmpty(telp)) {
            Toast.makeText(this, "Isi Data Telepon", Toast.LENGTH_LONG).show();
        } else if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, "Isi Data Email", Toast.LENGTH_LONG).show();
        } else if (TextUtils.isEmpty(kelas)) {
            Toast.makeText(this, "Pilih Kelas", Toast.LENGTH_LONG).show();
        }  else if (TextUtils.isEmpty(tipe)) {
            Toast.makeText(this, "Pilih Tipe Perusahaan", Toast.LENGTH_LONG).show();
        } else {
            String uid = databasePerusahaan.push().getKey();

            Form form = new Form(uid, nama, nis, alamat, telp, email, kelas, tipe);

            databasePerusahaan.child(uid).setValue(form);

            Toast.makeText(this, "Silahkan Pilih Perusahaan", Toast.LENGTH_LONG).show();


            startActivity(new Intent(FormActivity.this, Data.class));

        }

    }

}
